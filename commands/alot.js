// Ported from https://github.com/github/hubot-scripts/blob/master/src/scripts/alot.coffee

var images;

images = ["http://4.bp.blogspot.com/_D_Z-D2tzi14/S8TRIo4br3I/AAAAAAAACv4/Zh7_GcMlRKo/s400/ALOT.png", "http://3.bp.blogspot.com/_D_Z-D2tzi14/S8TTPQCPA6I/AAAAAAAACwA/ZHZH-Bi8OmI/s1600/ALOT2.png", "http://2.bp.blogspot.com/_D_Z-D2tzi14/S8TiTtIFjpI/AAAAAAAACxQ/HXLdiZZ0goU/s320/ALOT14.png", "http://fc02.deviantart.net/fs70/f/2010/210/1/9/Alot_by_chrispygraphics.jpg"];

module.exports = function(commander,logger) {

  commander.script({
    help: 'Alot spy'
  });


	commander.spy({
		hear: /(^|\W)alot(\z|\W|$)/i,
		help: 'listens for the word alot and displays an image of one',
		action: function(event,response) {
			return response.send(response.random(images));
		}
	});
};
